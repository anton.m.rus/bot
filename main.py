# -*- coding: UTF-8 -*-

import logging
from aiogram import Bot, Dispatcher, executor, types

import config as cfg
from db import Database
from swear import get_swears


logging.basicConfig(level=logging.INFO)

bot = Bot(token=cfg.TOKEN)
dp = Dispatcher(bot)
db = Database('database.db')

@dp.message_handler(commands=['mute'], commands_prefix='/')
async def mute(message: types.Message):
    if str(message.from_user.id) in cfg.ADMINS:
        if not message.reply_to_message:
            await message.reply('Эта команда должна быть ответом на сообщение')
            return
        try:
            mute_time = int(message.text[6:])
        except ValueError:
            await message.reply_to_message.reply('Нужно добавить время бана в минутах')
        try:
            db.add_mute(message.from_user.id, mute_time)
            await message.bot.delete_message(cfg.CHAT_ID, message.message_id)
            await message.reply_to_message.reply(f'@{message.from_user.username} вам нельзя писать сообщения в этот чат {mute_time} минут')
        except UnboundLocalError:
            return

@dp.message_handler(content_types=['new_chat_members'])
async def user_joined(message: types.message):
    await message.answer(f'@{message.from_user.username}\nДобро пожаловать, {message.from_user.first_name}!\n{cfg.START_MESSAGE}')


@dp.message_handler()
async def mess_handler(message: types.Message):
    if not db.user_exists(message.from_user.id):
        db.add_user(message.from_user.id)
    if not db.is_mute(message.from_user.id):
        text = message.text.lower()
        swears = get_swears()
        for word in text.strip().split():
            if word in swears:
                await message.answer(f'@{message.from_user.username} Использовать ненормативную лексику запрещено', reply=True)
                await message.delete()
    else:
        await message.delete()

if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)