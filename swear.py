
def get_swears():
    with open('swears.txt', 'r', encoding='UTF-8') as f:
        words = f.read().split(', ')
    return words

if __name__ == '__main__':
    get_swears()